<?php

namespace App\Http\Controllers;

use App\jenisTabungan;
use Illuminate\Http\Request;
use Alert;

class JenisTabunganController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jenis_tabungan = jenisTabungan::all();
        $data['active_menu'] = 'jenis';
        return view('jenis_tabungan.index', compact('jenis_tabungan', 'data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['active_menu'] = 'jenis';
        return view('jenis_tabungan.create', compact('data'));
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            "nama" => 'required',
            "keterangan" => 'required'
        ],[
            "nama.required" => "Nama Tabungan harus diisi",
            "keterangan.required" => "Keterangan Tabungan harus diisi"
        ]);

        jenisTabungan::firstOrCreate([
            "nama" => $request->nama,
            "keterangan" => $request->keterangan
        ]);

        return redirect('/jenis-tabungan')->with('success', 'Jenis Tabungan Berhasil disimpan!');
    }


    public function show($id)
    {
        $jenis_tabungan = jenisTabungan::find($id);
        $data['active_menu'] = 'jenis';
        return view('jenis_tabungan.show', compact('jenis_tabungan', 'data'));
    }


    public function edit($id)
    {
        $jenis_tabungan = jenisTabungan::find($id);
        $data['active_menu'] = 'jenis';
        return view('jenis_tabungan.edit', compact('jenis_tabungan', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\jenisTabungan  $jenisTabungan
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $this->validate($request,[
            "nama" => 'required',
            "keterangan" => 'required'
        ],[
            "nama.required" => "Nama Tabungan harus diisi",
            "keterangan.required" => "Keterangan Tabungan harus diisi"
        ]);

        jenisTabungan::where('id',$id)->update([
            "nama" => $request->nama,
            "keterangan" => $request->keterangan
        ]);

        return redirect('/jenis-tabungan')->with('success', 'Jenis Tabungan Berhasil diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\jenisTabungan  $jenisTabungan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        jenisTabungan::destroy($id);
        return redirect('/jenis-tabungan')->with('success', "Jenis Tabungan Berhasil dihapus!");
    }
}
