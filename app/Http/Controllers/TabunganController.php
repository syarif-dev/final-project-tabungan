<?php

namespace App\Http\Controllers;

use App\jenisTabungan;
use App\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Tabungan;
use App\Transaksi;

class TabunganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tabungan = Tabungan::all();
        $data['active_menu'] = 'tabungan';
        return view('tabungan.index', compact('tabungan','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $member = Member::all();
        $jenis_tabungan = jenisTabungan::all();
        $data['active_menu'] = 'tabungan';
        return view('tabungan.create', compact('member','jenis_tabungan','data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
            'no_rekening' => 'required',
            'saldo_awal' => 'required',
            'tanggal_registrasi' => 'required',
            'member_id' => 'required',
            'jenis_tabungan_id' => 'required',
            ],
            [
            'no_rekening.required' => 'No_Rekening harus diisi',
            'saldo_awal.required'  => 'Saldo Awal harus diisi',
            'tanggal_registrasi.required'  => 'Tanggal Registrasi harus diisi',
            'member_id' => 'Member Harus diisi',
            'jenis_tabungan_id' => 'Jenis tabungan harus diisi'
            ]
        );

        $tabungan = new Tabungan;
        $tabungan->no_rekening = $request->no_rekening;
        $tabungan->saldo_awal = $request->saldo_awal;
        $tabungan->saldo = $request->saldo_awal;
        $tabungan->tanggal_registrasi = $request->tanggal_registrasi;
        $tabungan->member_id = $request->member_id;
        $tabungan->jenis_tabungan_id = $request->jenis_tabungan_id;


        $tabungan->save();

        $transaksi = [
            'tanggal_transaksi' => now()->format('Y-m-d'),
            'kode_transaksi' => uniqid(),
            'tipe' => 0,
            'jumlah' => $request->saldo_awal,
            'tabungan_id' => $tabungan->id
        ];
        Transaksi::create($transaksi);

        return redirect('/tabungan')->with('success', 'Data tabungan berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['data'] = Tabungan::with(['transaksi', 'member', 'jenistabungan'])->findOrFail($id);
        $data['active_menu'] = 'tabungan';

        return view ('tabungan.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['data'] = Tabungan::find($id);
        $data['active_menu'] = 'tabungan';
        $data['jenis'] = jenisTabungan::all();
        return view ('tabungan.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'no_rekening' => 'required|numeric',
            'jenis_tabungan_id' => 'required||numeric'
        ]);
        Tabungan::where('id', $id)->update([
            'no_kredit' => $request->no_kredit,
            'jenis_tabungan_id' => $request->jenis_tabungan_id,
        ]);
        return redirect('/tabungan')->with('success', 'Data tabungan berhasil diperbaharui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tabungan::where('id', $id)->delete();
        Transaksi::where('tabungan_id', $id)->delete();
        return redirect('/tabungan')->with('success', 'Data tabungan berhasil dihapus!');
    }
}
