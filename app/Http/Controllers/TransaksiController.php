<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaksi;
use App\Tabungan;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaksi = Transaksi::with('tabungan')->get();
        $data['active_menu'] = 'transaksi';
        return view('transaksi.index', compact('transaksi','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $data['active_menu'] = 'tabungan';
        $data['data'] = Tabungan::find($id);
        return view('transaksi.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $request->validate(
            [
                'tanggal_transaksi' => 'required',
                'kode_transaksi' => 'required',
                'tipe' => 'required',
                'jumlah' => 'required|numeric',
            ],
            [
                'tanggal_transaksi.required' => 'Tanggal Transaksi harus diisi',
                'kode_transaksi.required'  => 'Kode Transaksi harus diisi',
                'tipe.required'  => 'Tipe Transaksi harus diisi',
                'jumlah' => 'Jumlah harus diisi',
            ]
        );

        $transaksi = [
            'tanggal_transaksi' => $request->tanggal_transaksi,
            'kode_transaksi' => $request->kode_transaksi,
            'tipe' => $request->tipe,
            'jumlah' => $request->jumlah,
            'tabungan_id' => $id
        ];

        Transaksi::create($transaksi);

        $tabungan = Tabungan::find($id);
        if($request->tipe){
            $tabungan->saldo -= $request->jumlah;
        }else{
            $tabungan->saldo += $request->jumlah;
        }
        $tabungan->update();

        return redirect()->route('tabungan.show', $id)->with('success', 'Data transaksi berhasil ditambahkan!');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::findOrFail($id);

        return view ('film.show', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
