<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class MemberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['data'] = Member::all();
        $data['active_menu'] = 'member';
        return view('member.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['active_menu'] = 'member';
        return view('member.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode' => 'required',
            'nama' => 'required',
            'jenis_kelamin' => 'required||numeric',
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);
        $user = [
            'name' => $request->nama,
            'email' => $request->email,
            'email_verified_at' => DB::raw('NOW()'),
            'password' => Hash::make($request->password),
            'level' => 'anggota',
            'created_at' => DB::raw('NOW()'),
            'updated_at' => DB::raw('NOW()'),
        ];
        $newUser = User::create($user);
        $member = [
            'nama' => $request->nama,
            'jenis_kelamin' => $request->jenis_kelamin,
            'alamat' => $request->alamat,
            'kode' => $request->kode,
            'user_id' => $newUser->id
        ];
        Member::create($member);
        return redirect('/member')->with('success', 'Data member berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['member'] = Member::with('user')->findOrFail($id);
        $data['active_menu'] = 'member';
        return view('member.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['member'] = Member::with('user')->findOrFail($id);
        $data['active_menu'] = 'member';
        return view('member.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $member = Member::findOrFail($id);
        $request->validate([
            'kode' => 'required',
            'nama' => 'required',
            'jenis_kelamin' => 'required||numeric',
            'email' => 'required|email',
            'password' => 'nullable|min:6',
        ]);
        $user = [
            'name' => $request->nama,
            'email' => $request->email,
            'level' => 'anggota',
            'updated_at' => DB::raw('NOW()'),
        ];
        if($request->has('password') && $request->password != ''){
            $user['password'] = Hash::make($request->password);
        }
        $newUser = User::where('id',$member->user_id)->update($user);
        $member = [
            'nama' => $request->nama,
            'jenis_kelamin' => $request->jenis_kelamin,
            'alamat' => $request->alamat,
            'kode' => $request->kode
        ];
        Member::where('id', $id)->update($member);
        return redirect('/member')->with('success', 'Data member berhasil diperbaharui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Member::findOrFail($id);
        User::where('id', $member->user_id)->delete();
        $member->delete();
        return redirect('/member')->with('success', "Data member berhasil dihapus!");
    }
}
