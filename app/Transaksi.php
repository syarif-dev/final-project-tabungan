<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = "transaksi";
    protected $fillable = ["tanggal_transaksi","kode_transaksi","tipe","jumlah","tabungan_id"];

    public function tabungan()
    {
        return $this->belongsTo('App\Tabungan', 'tabungan_id');
    }
}
