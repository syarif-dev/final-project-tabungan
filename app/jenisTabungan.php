<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jenisTabungan extends Model
{
    protected $table = 'jenis_tabungan';

    protected $fillable = [
        'nama', 'keterangan'
    ];

    // public function tabungan(){
    //     return $this->hasMany(Tabungan::class);
    // }

    public function tabungan()
    {
        return $this->hasMany('App\Tabungan');
    }
}
