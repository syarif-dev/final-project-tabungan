<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tabungan extends Model
{
    protected $table = "tabungan";
    protected $fillable = ["no_rekening","saldo_awal","saldo","tanggal_registrasi","member_id", "jenis_tabungan_id"];

    public function member()
    {
        return $this->belongsTo('App\Member', 'member_id');
    }

    public function jenistabungan()
    {
        return $this->belongsTo('App\jenisTabungan', 'jenis_tabungan_id');
    }

    /**
     * Get all of the transaksi for the Tabungan
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transaksi()
    {
        return $this->hasMany(Transaksi::class)->orderBy('tanggal_transaksi', 'ASC');
    }

}
