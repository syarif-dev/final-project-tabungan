# Final Project Tabungan

## Kelompok 21

<pre>
Nama    : Rijwan
Email   : 7rijwan1998@gmail.com

Nama    : Syarif Hidayatulloh
Email   : syarif120915@gmail.com

Nama    : Tri Fuad Aziz
Email   : azizfuadtri@gmail.com
</pre>

## ERD

<p align="center"><img src="erd.png" width="400"></p>

## Link Deploy

<h2><a href="https://final-project-tabungan.herokuapp.com" target="_blank">Aplikasi Tabungan</a></h2>
<pre>
Username: admin@gmail.com
Password: admin123
</pre>

<pre>
<h2>Template yang digunakan : Admin LTE 3</h2>
<h2>Library yang digunakan : DataTables, SweetAlert, DatePicker</h2>
<h2>Deskripsi: </h2>
<p>Website tabungan yang dikelola oleh admin, admin dapat melakukan :</p>
<ul>
<li>Menambah, mengedit, dan menghapus member</li>
<li>Menambah, mengedit, dan menghapus jenis tabungan</li>
<li>Menambah, mengedit, dan menghapus tabungan member</li>
<li>Menambah, mengedit, dan menghapus transaksi setiap member</li>
</ul>
</pre>
