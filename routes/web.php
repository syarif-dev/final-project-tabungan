<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('logout', 'Auth\LoginController@logout');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('/jenis-tabungan', 'JenisTabunganController' );
Route::resource('/member', 'MemberController' );
Route::resource('/transaksi', 'TransaksiController');
Route::resource('/tabungan', 'TabunganController');
Route::get('/tabungan/{id}/transaksi', 'TransaksiController@create')->name('tabungan.transaksi.create');
Route::post('/tabungan/{id}/transaksi', 'TransaksiController@store')->name('tabungan.transaksi.store');
