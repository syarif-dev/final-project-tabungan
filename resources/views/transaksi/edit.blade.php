@extends('template.master')

@section('title', 'Transaksi')

@section('card-title')
<h3 class="card-title">Edit Transaksi</h3>
@endsection

@push('style')
    <link rel="stylesheet" type="text/css" href="{{asset('https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('datepicker/css/datepicker.css')}}"/>
@endpush

@section('content')
<form action="/transaksi/{{ $transaksi->id }}" method="POST" id="form">
    @csrf
    @method('PUT')
    <div class="card-body">
        <div class="form-group">
            <label>Tanggal Transaksi</label>
            <div class="form-group">
                <input type="text"  name="tanggal_transaksi" placeholder="Masukkan Tanggal Transaksi"  class="form-control datepicker" value="{{ $transaksi->tanggal_transaksi }}"  required>
            </div> 
            @error('tanggal_transaksi')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label>Kode Transaksi</label>
            <input type="text" class="form-control" name="kode_transaksi" id="nama" placeholder="Masukkan Kode Transaksi" value="{{ $transaksi->kode_transaksi }}"/>
            @error('kode_transaksi')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label>Tipe Transaksi</label> <br>
            <input type="radio" name="tipe" value="0" checked> Debit
            <input type="radio" name="tipe" value="1"> Kredit
            @error('tipe')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label for="jumlah">Jumlah Transaksi</label>
            <input type="number" class="form-control" name="jumlah"id="nama"placeholder="Masukkan Jumlah Transaksi" value="{{ old('jumlah', '') }}"/>
            @error('jumlah')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror   
        </div>

    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update Jenis Tabungan</button>
    </div>
</form>
@endsection

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="{{asset('datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script type="text/javascript">
        $(function() {
            $(".datepicker").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayHighlight: true,
            });
        });
    </script>
@endpush



