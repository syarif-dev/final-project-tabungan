@extends('template.master')

@section('title', 'Transaksi')

@section('card-title')
    <h3 class="card-title">Tambah Transaksi</h3>
@endsection

@push('style')
    <link rel="stylesheet" type="text/css" href="{{asset('https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('datepicker/css/datepicker.css')}}"/>
@endpush

@section('content')
    <form action="{{ route('tabungan.transaksi.store', $data['data']->id) }}" method="POST" id="form">
        @csrf
        <div class="card-body">

            <div class="form-group">
                <label>No Rekening</label>
                <div class="form-group">
                    <input type="text" value="{{ $data['data']->no_rekening }}" disabled class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label>Tanggal Transaksi</label>
                <div class="form-group">
                    <input type="text"  name="tanggal_transaksi" placeholder="Masukkan Tanggal Transaksi"  class="form-control datepicker"  required>
                </div>
                @error('tanggal_transaksi')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label>Kode Transaksi</label>
                <input type="text" class="form-control" name="kode_transaksi" id="nama" value="{{ uniqid() }}" placeholder="Masukkan Kode Transaksi"/>
            </div>
            @error('kode_transaksi')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <div class="form-group">
                <label>Tipe Transaksi</label> <br>
                <input type="radio" name="tipe" value="0" checked> Penambahan
                <input type="radio" name="tipe" value="1"> Pengurangan
                @error('tipe')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="jumlah">Jumlah Transaksi</label>
                <input type="number" class="form-control" name="jumlah" id="nama" placeholder="Masukkan Jumlah Transaksi"/>
                @error('jumlah')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Tambah Transaksi</button>
        </div>
    </form>

@endsection

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="{{asset('datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script type="text/javascript">
        $(function() {
            $(".datepicker").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayHighlight: true,
            });
        });
    </script>
@endpush


