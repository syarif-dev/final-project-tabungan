@extends('template.master')

@section('title', 'Transaksi')

@section('card-title')
    <h3 class="card-title">Transaksi</h3>
@endsection

@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css"/>
@endpush

@section('content')

    @if(session('success'))
        <div class="alert alert-success my-2 mx-2">
            {{ session('success') }}
        </div>
    @endif

    <h2>List Transaksi</h2>

    <div class="mx-2">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>No Rekening</th>
                    <th>Tanggal Transaksi</th>
                    <th>Kode Transaksi</th>
                    <th>Tipe Transaksi</th>
                    <th>Jumlah</th>
                </tr>
            </thead>
            <tbody>

            @forelse ($transaksi as $key => $item)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $item->tabungan->no_rekening}}</td>
                    <td>{{ $item->tanggal_transaksi}}</td>
                    <td>{{ $item->kode_transaksi}}</td>
                    <td>{{ $item->tipe ? 'Pengurangan' : 'Penambahan'}}</td>
                    <td>Rp{{ number_format($item->jumlah) }}</td>
                </tr>
            @empty
                <h2>Tidak ada Transaksi yang ditemukan</h2>
            @endforelse

            </tbody>

            <tfoot>
                <tr>
                    <th>No</th>
                    <th>Tanggal Transaksi</th>
                    <th>Kode Transaksi</th>
                    <th>Tipe</th>
                    <th>Jumlah</th>
                </tr>
            </tfoot>
        </table>
    </div>


@endsection

@push('scripts')
    <script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
        $(function () {
            $("#example1").DataTable();
        });
    </script>
@endpush
