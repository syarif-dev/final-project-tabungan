@extends('template.master')

@section('title', 'Tabungan')

@section('card-title')
    <h3 class="card-title">Detail Tabungan</h3>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-2">No Rekening</div>
        <div class="col-sm">{{ $data['data']->no_rekening }}</div>
    </div>
    <div class="row">
        <div class="col-sm-2">Member</div>
        <div class="col-sm">{{ $data['data']->member->nama }}</div>
    </div>
    <div class="row">
        <div class="col-sm-2">Jenis Tabungan</div>
        <div class="col-sm">{{ $data['data']->jenistabungan->nama }}</div>
    </div>
    <div class="row">
        <div class="col-sm-2">Tanggal Registrasi</div>
        <div class="col-sm">{{ $data['data']->tanggal_registrasi }}</div>
    </div>
    <div class="row">
        <div class="col-sm-2">Saldo Awal</div>
        <div class="col-sm">Rp{{ number_format($data['data']->saldo_awal) }}</div>
    </div>
    <div class="row">
        <div class="col-sm-2">Saldo</div>
        <div class="col-sm">Rp{{ number_format($data['data']->saldo) }}</div>
    </div>
    <div class="d-flex justify-content-between">
        <h3>Transaksi</h3>
        <a href="{{ route('tabungan.transaksi.create', $data['data']->id) }}" class="btn btn-success">Tambah Transaksi</a>
    </div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th>Tanggal Transaksi</th>
                <th>Kode Transaksi</th>
                <th>Tipe</th>
                <th>Jumlah</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data['data']->transaksi as $key => $value)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $value->tanggal_transaksi }}</td>
                    <td>{{ $value->kode_transaksi }}</td>
                    <td>{{ $value->tipe ? 'Pengurangan' : "Penambahan" }}</td>
                    <td>Rp{{ number_format($value->jumlah) }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection
