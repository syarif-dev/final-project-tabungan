@extends('template.master')

@section('title', 'Tabungan')

@section('card-title')
    <h3 class="card-title">Edit Tabungan</h3>
@endsection

@push('style')
    <link rel="stylesheet" type="text/css" href="{{asset('https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('datepicker/css/datepicker.css')}}"/>
@endpush

@section('content')
    <form action="{{ route('tabungan.update', $data['data']->id) }}" method="POST" id="form">
        @csrf
        @method('PUT')
        <div class="card-body">

            <div class="form-group">
                <label>Jenis Tabungan</label>
                <select name="jenis_tabungan_id" id="" class="form-control">
                    @forelse ($data['jenis'] as $key => $item)
                        <option value="{{$item->id}}" {{ $item->id == $data['data']->jenis_tabungan_id ? 'selected' : '' }}>{{$item->nama}}</option>
                    @empty
                        <option value="">---Tidak ada Data Jenis Tabungan--</option>
                    @endforelse
                </select>
                @error('jenis_tabungan')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>


            <div class="form-group">
                <label for="no_rekening">No. Rekening</label>
                <input type="number" class="form-control" name="no_rekening" id="no-rekening" placeholder="Masukkan Nomor Rekening" value="{{ $data['data']->no_rekening }}"/>
                @error('no_rekening')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Update Tabungan</button>
        </div>
    </form>

@endsection

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="{{asset('datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script type="text/javascript">
        $(function() {
            $(".datepicker").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayHighlight: true,
            });
        });
    </script>
@endpush
