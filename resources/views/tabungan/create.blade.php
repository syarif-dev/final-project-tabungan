@extends('template.master')

@section('title', 'Tabungan')

@section('card-title')
    <h3 class="card-title">Buat Tabungan</h3>
@endsection

@push('style')
    <link rel="stylesheet" type="text/css" href="{{asset('https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('datepicker/css/datepicker.css')}}"/>
@endpush

@section('content')
    <form action="/tabungan" method="POST" id="form">
        @csrf
        <div class="card-body">

            <div class="form-group">
                <label>Nama member</label>
                <select name="member_id" id="" class="form-control">
                    @forelse ($member as $key => $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                    @empty
                        <option value="">--Tidak ada Data Member--</option>
                    @endforelse
                </select>
                @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>


            <div class="form-group">
                <label>Jenis Tabungan</label>
                <select name="jenis_tabungan_id" id="" class="form-control">
                    @forelse ($jenis_tabungan as $key => $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                    @empty
                        <option value="">---Tidak ada Data Jenis Tabungan--</option>
                    @endforelse
                </select>
                @error('jenis_tabungan')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>


            <div class="form-group">
                <label for="no_rekening">No. Rekening</label>
                <input type="number" class="form-control" name="no_rekening" id="no-rekening" placeholder="Masukkan Nomor Rekening" value="{{ old('no_rekening', '') }}"/>
                @error('no_rekening')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>


            <div class="form-group">
                <label for="saldo_awal">Saldo Awal</label>
                <input type="text" class="form-control" name="saldo_awal" id="saldo-awal" placeholder="Masukkan Saldo Awal" value="{{ old('saldo_awal', '') }}"/>
                @error('saldo_awal')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>


            <div class="form-group">
                <label for="tanggal_registrasi">Tanggal Registrasi</label>
                <div class="form-group">
                    <input type="text"  name="tanggal_registrasi"  class="form-control datepicker"  required>
                </div>
                @error('tanggal_registrasi')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Buat Tabungan</button>
        </div>
    </form>

@endsection

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="{{asset('datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script type="text/javascript">
        $(function() {
            $(".datepicker").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayHighlight: true,
            });
        });
    </script>
@endpush
