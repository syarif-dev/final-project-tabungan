@extends('template.master')

@section('title', 'Tabungan')

@section('card-title')
    <h3 class="card-title">Daftar Tabungan</h3>
@endsection

@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css"/>
@endpush

<!-- library datepicker-->
@push('style')

@endpush

@section('content')

    @if(session('success'))
        <div class="alert alert-success my-2 mx-2">
            {{ session('success') }}
        </div>
    @endif

    <div>
        <a class="btn btn-primary ml-2 my-2" href="/tabungan/create">Buat Tabungan</a>
    </div>

    <div class="mx-2">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>No. Rekening</th>
                    <th>Saldo</th>
                    <th>Tanggal Registrasi</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>

            @forelse ($tabungan as $key => $item)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $item->no_rekening}}</td>
                    <td>Rp {{ number_format($item->saldo, 0, ',', '.') }}</td>
                    <td>{{ $item->tanggal_registrasi}}</td>

                    <td>
                        {{-- <a class="btn btn-info btn-sm" href="/transaksi/create">Tambah Transaksi</a> --}}
                        <form action="{{ route('tabungan.destroy', $item->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a class="btn btn-info btn-sm" href="/tabungan/{{ $item->id }}">Detail</a>
                            <a class="btn btn-warning btn-sm" href="/tabungan/{{ $item->id }}/edit">Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>

                </tr>
            @empty
                <h2>Tidak ada Daftar Tabungan yang ditemukan</h2>
            @endforelse

            </tbody>

            <tfoot>
                <tr>
                    <th>No</th>
                    <th>No. Rekening</th>
                    <th>Saldo</th>
                    <th>Tanggal Registrasi</th>
                    <th>Action</th>
                </tr>
            </tfoot>
        </table>
    </div>


@endsection

@push('scripts')
    <script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
        $(function () {
            $("#example1").DataTable();
        });
    </script>
@endpush
