@extends('template.master')

@section('title', 'Detail')

@section('card-title')
<h3 class="card-title">Detail Jenis Tabungan {{ $jenis_tabungan->nama }}</h3>
@endsection

@section('content')
<div>
    <a class="btn btn-secondary ml-2 my-2" href="/jenis-tabungan">Back</a>
</div>
<div class="flex flex-col justify-center mx-2">
    <div>
        <h1 class="text-center font-bold my-8 text-xl ">
            Detail {{ $jenis_tabungan->nama }}
        </h1>
    </div>
    <div class="bg-gray-100 shadow-xl p-12 mx-6 my-6">
        <p class="m-6 text-2xl font-bold  ">
            Nama : {{ $jenis_tabungan->nama }}
        </p>
        <p class="m-6 text-2xl font-bold  ">
            Keterangan : {{ $jenis_tabungan->keterangan }}
        </p>
    </div>
</div>

@endsection
