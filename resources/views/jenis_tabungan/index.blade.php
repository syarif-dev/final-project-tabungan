@extends('template.master')

@section('title', 'Jenis Tabungan')

@section('card-title')
<h3 class="card-title">Jenis Tabungan</h3>
@endsection

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css"/>
@endpush

@section('content')

<div>
    <a class="btn btn-primary ml-2 my-2" href="/jenis-tabungan/create">Tambah Jenis Tabungan</a>
</div>
<div class="mx-2">
    <table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>No.</th>
            <th>Nama </th>
            <th>keterangan</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($jenis_tabungan as $key => $item )
        <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $item->nama }}</td>
            <td>{{ $item->keterangan }}</td>
            <td>
                <form action="/jenis-tabungan/{{ $item->id }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a class="btn btn-info btn-sm" href="/jenis-tabungan/{{ $item->id }}">Detail</a>
                    <a class="btn btn-warning btn-sm" href="/jenis-tabungan/{{ $item->id }}/edit">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
        @empty
        <h2 class="ml-2">Tidak adan jenis tabungan yang ditemukan</h2>
        @endforelse
    </tbody>
    <tfoot>
        <tr>
            <th>No.</th>
            <th>Nama </th>
            <th>Keterangan</th>
            <th>Action</th>
        </tr>
    </tfoot>
</table>
</div>


@endsection

@push('scripts')
<script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
    $(function () {
        $("#example1").DataTable();
    });
</script>
@endpush
