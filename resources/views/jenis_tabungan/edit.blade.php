@extends('template.master')

@section('title', 'Jenis Tabungan')

@section('card-title')
<h3 class="card-title">Edit Jenis Tabungan</h3>
@endsection
@section('content')
<form action="/jenis-tabungan/{{ $jenis_tabungan->id }}" method="POST" id="form">
    @csrf
    @method('PUT')
    <div class="card-body">
        <div class="form-group">
            <label for="nama">Nama</label>
            <input
                type="text"
                class="form-control"
                name="nama"
                id="nama"
                placeholder="Masukkan Nama Tabungan"
                value="{{ $jenis_tabungan->nama }}"
            />
            @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="keterangan">Keterangan</label>
            <textarea
                type="text"
                class="form-control"
                name="keterangan"
                id="keterangan"
                placeholder="Masukkan keterangan Tabungan"
            >{{ $jenis_tabungan->keterangan }}</textarea>
            @error('keterangan')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update Jenis Tabungan</button>
    </div>
</form>

@endsection
