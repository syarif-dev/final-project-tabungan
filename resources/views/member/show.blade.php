@extends('template.master')

@section('title', 'Member')

@section('card-title')
    <h3 class="card-title">Detail Member</h3>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-2">Nama</div>
        <div class="col-sm">{{ $data['member']->nama }}</div>
    </div>
    <div class="row">
        <div class="col-sm-2">Kode</div>
        <div class="col-sm">{{ $data['member']->kode }}</div>
    </div>
    <div class="row">
        <div class="col-sm-2">Email</div>
        <div class="col-sm">{{ $data['member']->user->email }}</div>
    </div>
    <div class="row">
        <div class="col-sm-2">Alamat</div>
        <div class="col-sm">{{ $data['member']->alamat }}</div>
    </div>
    <div class="row">
        <div class="col-sm-2">Jenis Kelamin</div>
        <div class="col-sm">{{ $data['member']->jenis_kelamin ? "Laki-laki" : "Perempuan" }}</div>
    </div>
@endsection
