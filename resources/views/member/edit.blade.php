@extends('template.master')

@section('title', 'Member')

@section('card-title')
    <h3 class="card-title">Edit Member</h3>
@endsection

@section('content')

    @if(session('errors'))
        <div class="alert alert-danger my-2 mx-2">
            {{ session('errors') }}
        </div>
    @endif

    <form action="{{ route('member.update', $data['member']->id) }}" method="post">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label for="kode">Kode</label>
                <input type="text"
                    class="form-control"
                    id="kode"
                    placeholder="Masukan kode"
                    name="kode"
                    value="{{ $data['member']->kode }}">
                @error('kode')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="nama">Nama</label>
                <input
                    type="text"
                    class="form-control"
                    id="nama"
                    placeholder="Masukan nama"
                    name="nama"
                    value="{{ $data['member']->nama }}">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="nama">Jenis Kelamin</label> <br>
                <input type="radio" name="jenis_kelamin" value="0" {{ $data['member']->jenis_kelamin == 0 ? 'checked' : '' }}> Perempuan
                <input type="radio" name="jenis_kelamin" value="1" {{ $data['member']->jenis_kelamin == 1 ? 'checked' : '' }}> Laki Laki
                @error('jenis_kelamin')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea class="form-control" rows="3" placeholder="Masukan alamat" name="alamat">{{ $data['member']->alamat }}</textarea>
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" placeholder="Masukan email" name="email" value="{{ $data['member']->user->email }}">
                @error('email')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" placeholder="Masukan password" name="password">
                @error('password')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        <div class="card-footer text-center">
            <button type="submit" name="simpan" class="btn btn-info">Update</button>
        </div>
    </form>


@endsection
