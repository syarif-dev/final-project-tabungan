@extends('template.master')

@section('title', 'Member')

@section('card-title')
    <h3 class="card-title">Tambah Member</h3>
@endsection

@section('content')

    @if(session('error'))
        <div class="alert alert-danger my-2 mx-2">
            {{ session('error') }}
        </div>
    @endif

    <form action="{{ route('member.store') }}" method="post">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="kode">Kode</label>
                <input type="text" class="form-control" id="kode" placeholder="Masukan kode" name="kode">
                @error('kode')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" placeholder="Masukan nama" name="nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="nama">Jenis Kelamin</label> <br>
                <input type="radio" name="jenis_kelamin" value="0" checked> Perempuan
                <input type="radio" name="jenis_kelamin" value="1"> Laki Laki
                @error('jenis_kelamin')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea class="form-control" rows="3" placeholder="Masukan alamat" name="alamat"></textarea>
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" placeholder="Masukan email" name="email">
                @error('email')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" placeholder="Masukan password" name="password">
                @error('password')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
        <div class="card-footer text-center">
            <button type="submit" name="simpan" class="btn btn-info">Simpan</button>
        </div>
    </form>


@endsection
