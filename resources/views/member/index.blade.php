@extends('template.master')

@section('title', 'Member')

@section('card-title')
    <h3 class="card-title">Member</h3>
@endsection

@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css"/>
@endpush

@section('content')

    @if(session('success'))
        <div class="alert alert-success my-2 mx-2">
            {{ session('success') }}
        </div>
    @endif

    <div>
        <a class="btn btn-primary ml-2 my-2" href="{{ route('member.create') }}">Tambah Member</a>
    </div>
    <div class="mx-2">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kode</th>
                    <th>Nama</th>
                    <th>Jenis Kelamin</th>
                    <th>Alamat</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data['data'] as $key => $item )
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $item->kode }}</td>
                        <td>{{ $item->nama }}</td>
                        <td>{{ $item->jenis_kelamin ? 'Laki-Laki' : "Perempuan" }}</td>
                        <td>{{ $item->alamat }}</td>
                        <td>
                            <form action="/member/{{ $item->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a class="btn btn-info btn-sm" href="/member/{{ $item->id }}">Detail</a>
                                <a class="btn btn-warning btn-sm" href="/member/{{ $item->id }}/edit">Edit</a>
                                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection

@push('scripts')
    <script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
        $(function () {
            $("#example1").DataTable();
        });
    </script>
@endpush
