<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img
            src="{{ asset('/adminlte/dist/img/AdminLTELogo.png') }}"
            alt="AdminLTE Logo"
            class="brand-image img-circle elevation-3"
            style="opacity: 0.8"
        />
        <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul
                class="nav nav-pills nav-sidebar flex-column"
                data-widget="treeview"
                role="menu"
                data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="/" class="nav-link {{ $data['active_menu'] == 'home' ? 'active' : '' }}">
                        <i class="nav-icon fas fa-home"></i>
                        <p>Home</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/member" class="nav-link {{ $data['active_menu'] == 'member' ? 'active' : '' }}">
                        <i class="nav-icon fas fa-user"></i>
                        <p>Member</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/jenis-tabungan" class="nav-link {{ $data['active_menu'] == 'jenis' ? 'active' : '' }}">
                        <i class="nav-icon fas fa-table"></i>
                        <p>Jenis Tabungan</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/transaksi" class="nav-link {{ $data['active_menu'] == 'transaksi' ? 'active' : '' }}">
                        <i class="nav-icon fas fa-credit-card"></i>
                        <p>List Transaksi</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="/tabungan" class="nav-link {{ $data['active_menu'] == 'tabungan' ? 'active' : '' }}">
                        <i class="nav-icon fas fa-university" aria-hidden="true"></i>
                        <p>Tabungan</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
